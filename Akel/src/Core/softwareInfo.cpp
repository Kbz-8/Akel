// This file is a part of Akel
// Authors : @kbz_8
// Created : 05/04/2021
// Updated : 13/05/2023

#include <Core/softwareInfo.h>

namespace Ak::Core
{
    bool isVulkanSupported()
    {
		uint32_t ver;
        if(vkEnumerateInstanceVersion(&ver) == VK_SUCCESS)
			return true;
        return false;
    }

    void printEngineHeader()
    {
        std::cout
		<< "    ___     __          __ 		" << '\n'
		<< "   /   |   / /__ ___   / /		" << '\n'
		<< "  / /| |  / //_// _ \\ / / 		" << '\n'
		<< " / ___ | / ,<  /  __// /  		" << '\n'
		<< "/_/  |_|/_/|_| \\___//_/  		" << '\n'
    	<< '\t' << '\t' << "By SpinWaves Studios - 2021-2023" << std::endl;
	}
}
