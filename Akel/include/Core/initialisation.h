// This file is a part of Akel
// Authors : @kbz_8
// Created : 06/10/2021
// Updated : 15/11/2022

#ifndef __AK_INITIALISATION__
#define __AK_INITIALISATION__

#include <Akpch.h>
#include <Core/instance.h>

namespace Ak
{
    bool initAkel(AkelInstance* project);
}

#endif // __AK_INITIALISATION__
