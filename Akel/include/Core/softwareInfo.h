// This file is a part of Akel
// Authors : @kbz_8
// Created : 05/04/2021
// Updated : 13/05/2023

#ifndef __AK_SOFTWARE_INFO__
#define __AK_SOFTWARE_INFO__

#include <Akpch.h>

namespace Ak::Core
{
    bool isVulkanSupported();
    void printEngineHeader();
}

#endif // __AK_SOFTWARE_INFO__
