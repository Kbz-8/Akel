// This file is a part of Akel
// Authors : @kbz_8
// Created : 06/05/2021
// Updated : 22/03/2023

#ifndef __AK_MODULES__
#define __AK_MODULES__

#include <Modules/ELTM/eltm.h>
#include <Modules/ImGui/imgui.h>
#include <Modules/Kila/kila.h>
#include <Modules/Scripting/script.h>
#include <Modules/Scripting/script_loader.h>
#include <Modules/Scripting/Lua/lua_loader.h>
#include <Modules/Scripting/Lua/lua_script.h>
#include <Modules/Scripting/Native/native_loader.h>
#include <Modules/Scripting/Native/native_script.h>

#endif // __AK_MODULES__
