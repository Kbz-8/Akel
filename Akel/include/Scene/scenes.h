// This file is a part of Akel
// Authors : @kbz_8
// Created : 05/12/2022
// Updated : 04/06/2023

#ifndef __AK_SCENES__
#define __AK_SCENES__

#include "scene.h"
#include "scene_manager.h"
#include "Cameras/first_person_3D.h"
#include "entity.h"
#include "entity_manager.h"
#include "scene_serializer.h"

#include "Attributes/attributes.h"

#endif
