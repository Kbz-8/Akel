// This file is a part of Akel
// Authors : @kbz_8
// Created : 28/03/2021
// Updated : 03/05/2023
//
// Akel is an open source game engine made for fun
// https://github.com/SpinWaves/Akel
// 
// MIT License
// 
// Copyright (c) 2021-2023 SpinWaves Studios
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef __AKEL__
#define __AKEL__

// AK_VERSION encoded like so XXYYZZ with XX be the major version, YY the minor and ZZ the patch version
#define AK_VERSION      000001
#define AK_VERSION_STR  "0.0.1 pre-alpha"

#include <Animation/animations.h>
#include <Audio/audio.h>
#include <Core/core.h>
#include <Graphics/graphics.h>
#include <Maths/maths.h>
#include <Modules/modules.h>
#include <Platform/platform.h>
#include <Renderer/renderer.h>
#include <Scene/scenes.h>
#include <Utils/utils.h>

#endif // __AKEL__
