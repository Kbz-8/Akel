// This file is a part of Akel
// Authors : @kbz_8
// Created : 11/03/2023
// Updated : 11/03/2023

#ifndef __AK_MODEL_FACTORY__
#define __AK_MODEL_FACTORY__

#include <Akpch.h>
#include "model.h"

namespace Ak
{
	Model createCube();
	Model createQuad();
}

#endif
