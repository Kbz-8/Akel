// This file is a part of Akel
// Authors : @kbz_8
// Created : 03/08/2021
// Updated : 28/03/2023

#ifndef __AK_AUDIO__
#define __AK_AUDIO__

#include <Audio/openAL.h>
#include <Audio/audioManager.h>
#include <Audio/buffer.h>
#include <Audio/sound.h>

#endif // __AK_AUDIO__
